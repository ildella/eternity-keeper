/**
 *  Eternity Keeper, a Pillars of Eternity save game editor.
 *  Copyright (C) 2015 Kim Mantas
 *
 *  Eternity Keeper is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  Eternity Keeper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.me.mantas.eternity.serializer;

// We host our own implementation of SharpSerializer which is used in
// Pillars of Eternity to serialize game objects into saves.

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import uk.me.mantas.eternity.Logger;
import uk.me.mantas.eternity.serializer.properties.*;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import org.slf4j.LoggerFactory;

import static java.util.Map.Entry;

public class SharpSerializer {
	private static final Logger logger = Logger.getLogger(SharpSerializer.class);

	public static final Map<String, Class> typeMap = TypeMap.map;
	private Map<Integer, Property> propertyCache = new HashMap<>();

	public static class Elements {
		public static final byte Collection = 1;
		public static final byte ComplexObject = 2;
		public static final byte Dictionary = 3;
		public static final byte MultiArray = 4;
		public static final byte Null = 5;
		public static final byte SimpleObject = 6;
		public static final byte SingleArray = 7;
		public static final byte ComplexObjectWithID = 8;
		public static final byte Reference = 9;
		public static final byte CollectionWithID = 10;
		public static final byte DictionaryWithID = 11;
		public static final byte SingleArrayWithID = 12;
		public static final byte MultiArrayWithID = 13;

		public static boolean isElementWithID(byte elementID) {
			return elementID == ComplexObjectWithID || elementID == CollectionWithID || elementID == DictionaryWithID
					|| elementID == SingleArrayWithID || elementID == MultiArrayWithID;
		}
	}

	private final File targetFile;
	private long position = 0;

	public SharpSerializer(String filePath) throws FileNotFoundException {
		targetFile = new File(filePath);
		if (!targetFile.exists()) {
			throw new FileNotFoundException();
		}
	}

	public Optional<Property> deserialize() {
		try {
			FileInputStream baseStream = new FileInputStream(targetFile);
			try (LittleEndianDataInputStream stream = new LittleEndianDataInputStream(baseStream)) {

				baseStream.getChannel().position(position);
				Deserializer deserializer = new Deserializer(stream, this);
				Property property = deserializer.deserialize();
				position = baseStream.getChannel().position();
				return Optional.ofNullable(createObject(property));
			}
		} catch (IOException e) {
			logger.error("Error opening target file '%s' for deserializing: %s%n", targetFile, e.getMessage());
		}

		return Optional.empty();
	}

	public void serialize(Property property) {
		try {
			FileOutputStream baseStream = new FileOutputStream(targetFile, true);

			try (LittleEndianDataOutputStream stream = new LittleEndianDataOutputStream(baseStream)) {

				baseStream.getChannel().position(baseStream.getChannel().size());

				Serializer serializer = new Serializer(stream);
				serializer.serialize(property);
			}
		} catch (IOException e) {
			logger.error("Error opening target file '%s' for serializing: %s%n", targetFile, e.getMessage());
		}
	}

	private Property createObject(Property property) {
		Property result = null;

		if (property == null) {
			String message = String.format("Property is null: %s%n", property);
			logger.error(message);
			return new NullProperty("");
		}
		if (property.type == null) {
			String message = String.format("Tried to create an object from a property with no type '%s'%n", property);
			logger.error(message);
			return new NullProperty("");
		}

		try {
			if (property instanceof NullProperty) {
				property.obj = null;
				result = property;
			} else if (property instanceof SimpleProperty) {
				result = createObjectFromSimpleProperty((SimpleProperty) property);
			} else if (property instanceof ReferenceTargetProperty) {
				ReferenceTargetProperty referenceProperty = (ReferenceTargetProperty) property;
				if (referenceProperty.reference != null && !referenceProperty.reference.isProcessed) {
					result = propertyCache.get(referenceProperty.reference.id);
				} else if (referenceProperty instanceof SingleDimensionalArrayProperty) {
					result = createObjectFromSingleDimensionalArrayProperty((SingleDimensionalArrayProperty) referenceProperty);
				} else {
					ComplexProperty complexProperty = (ComplexProperty) referenceProperty;
					Object instance = createInstance(property.type.type);
					if (complexProperty.reference != null) {
						propertyCache.put(complexProperty.reference.id, property);
					}
					fillProperties(instance, complexProperty.properties);
					if (complexProperty instanceof DictionaryProperty) {
						result = createObjectFromDictionaryProperty((DictionaryProperty) complexProperty, instance);
					} else if (complexProperty instanceof CollectionProperty) {
						result = createObjectFromCollectionProperty((CollectionProperty) complexProperty, instance);
					} else {
						result = createObjectFromComplexProperty(complexProperty, instance);
					}
				}
			}

		} catch (Exception e) {
			String message = String.format("Unimplemented property type '%s', '%s'", property.getClass(), property, e.getMessage());
			logger.error(message);
			throw new IllegalArgumentException(message, e);
		}
		return result;

	}

	private Property createObjectFromCollectionProperty(CollectionProperty property, Object instance) {

		try {
			Method addMethod = instance.getClass().getMethod("add", Object.class);

			for (Property item : property.items) {
				Property value = createObject(item);
				addMethod.invoke(instance, value.obj);
			}
		} catch (NoSuchMethodException e) {
			logger.error("Supposed 'Collection' class '%s' had no add method: %s%n",
					instance.getClass().getSimpleName(), e.getMessage());
		} catch (InvocationTargetException | IllegalAccessException e) {
			logger.error("Unable to call add method on class '%s': %s%n", instance.getClass().getSimpleName(),
					e.getMessage());
		}

		property.obj = instance;
		return property;
	}

	private Property createObjectFromDictionaryProperty(DictionaryProperty property, Object instance) {

		try {
			Method putMethod = instance.getClass().getMethod("put", Object.class, Object.class);

			for (Entry<Property, Property> item : property.items) {
				Property key = createObject(item.getKey());
				Property value = createObject(item.getValue());
				putMethod.invoke(instance, key.obj, value.obj);
			}
		} catch (NoSuchMethodException e) {
			logger.error("Supposed 'Dictionary' class '%s' had no put method: %s%n",
					instance.getClass().getSimpleName(), e.getMessage());
		} catch (InvocationTargetException | IllegalAccessException e) {
			logger.error("Unable to call put method on class '%s': %s%n", instance.getClass().getSimpleName(),
					e.getMessage());
		}

		property.obj = instance;
		return property;
	}

	private Property createObjectFromSingleDimensionalArrayProperty(SingleDimensionalArrayProperty property) {

		int itemsCount = property.items.size();
		Object[] array = new Object[itemsCount];

		if (property.reference != null) {
			propertyCache.put(property.reference.id, property);
		}

		for (int index = property.lowerBound; index < property.lowerBound + itemsCount; index++) {
			Property item = (Property) property.items.get(index);
			Property value = createObject(item);
			if (value != null) {
				array[index] = value.obj;
			}
		}

		property.obj = Arrays.copyOf(array, array.length, property.type.type);
		return property;
	}

	private Property createObjectFromComplexProperty(ComplexProperty property, Object instance) {

		property.obj = instance;
		return property;
	}

	private void fillProperties(Object obj, List properties) {
		// noinspection unchecked

		for (Property property : (List<Property>) properties) {
			Field field;

			try {
				field = obj.getClass().getField(property.name);
			} catch (NoSuchFieldException e) {
				logger.error("Class '%s' has no field '%s': %s%n", obj.getClass().getSimpleName(), property.name,
						e.getMessage());

				continue;
			}

			Property value = createObject(property);
			if (value == null) {
				continue;
			}

			try {
				field.set(obj, value.obj);
			} catch (IllegalAccessException | IllegalArgumentException e) {
				logger.error("Unable to set field '%s' of class '%s' and value '%s': %s%n", property.name,
						obj.getClass().getSimpleName(), value, e.getMessage());
			}
		}
	}

	private Object createInstance(Class type) {
		if (type == null) {
			throw new IllegalArgumentException();
		}

		try {
			return type.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			logger.error("Unable to instantiate object of type '%s': %s%n", type.getSimpleName(), e.getMessage());
			throw new IllegalArgumentException();
		}
	}

	private Property createObjectFromSimpleProperty(SimpleProperty property) {
		property.obj = property.value;
		return property;
	}
}
